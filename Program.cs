﻿using System;

namespace exersize24
{
    class Program
    {
        public static void PrintMessage( )
        {
            var a = $"My first method";
            Console.WriteLine(a);
        }
        public static void Main(string[] args)
        {
            PrintMessage( );
            PrintMessage( );
            PrintMessage( );
            PrintMessage( );
            PrintMessage( );
        }
        
        public static void PrintGreeting(string name)
        {
            Console.WriteLine($"Hello {name}");

        }
        public static void Second(string[] args)
        {
            PrintGreeting("Lisa");
            PrintGreeting("Mary");
            PrintGreeting("Lila");
            PrintGreeting("Felicia");
            PrintGreeting("Natalia");
        }
    }
}
